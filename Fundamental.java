package sample;

public class Fundamental {

    public int calculation (int a, int b, String operator ){

        switch (operator){
            case "+":
                return a+b;
            case "*":
                return a*b;
        }
        System.out.println(" " + operator);
        return 0;
    }
}
