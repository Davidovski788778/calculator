package sample;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import java.awt.event.ActionEvent;
import java.util.Scanner;


public class Controller {

    @FXML
    private Text output;
    private int sumbol1 = 0;
    private int sumbol2 = 0;

    private boolean start = true;

    private String operator = "";
    private Fundamental fundamental = new Fundamental();

    @FXML
    private void sumbol (ActionEvent event){
        if(start){
            output.setText("");//очищаем наше поле
            start = false;
        }
        Scanner number = new Scanner(System.in);
        int sumbol1=number.nextInt();
        int sumbol2=number.nextInt();
    }

    @FXML
    private void operation (ActionEvent event){
        String value = ((Button)event.getSource()).getText();
        if (!"=".equals(value)){//если наша операция не "равно"
            if(!operator.isEmpty()) return;//то мы ему присваиваем значение
            operator = value;
            sumbol1 = Integer.parseInt(output.getText());//получаем число которое введено первым
            output.setText("");//очищаем наше поле
        }
        else{
            if(operator.isEmpty()) return;
            output.setText(String.valueOf(fundamental.calculation(sumbol1,sumbol2,operator)));
            operator = "";
            start = true;
        }
    }

}
