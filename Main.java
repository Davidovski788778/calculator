package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));// загружаем сюда наш файл, который отвечает за интерфейс
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));  //тут отображается информация про наше окно
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
